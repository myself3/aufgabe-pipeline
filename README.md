# Aufgabe Pipeline

## Aufgabe 1: Vorbereitung

Für die Bearbeitung aller weiteren Aufgaben sind einige Vorbereitungen notwendig.

- [ ] [Erstelle](https://gitlab.com/users/sign_in#register-pane) dir zuerst einen Account bei Gitlab, falls du noch keinen besitzt. Du kannst dich statdessen auch einfach mit deinem Google, Github, ... Account [einloggen](https://gitlab.com/users/sign_in#register-pane).
- [ ] Um unsere Vorlage zu deinem Account hinzuzufügen, [fork](https://gitlab.com/devops-infomgmt/aufgabe-pipeline/-/forks/new) das Projekt.

Für die Bearbeitung folgender Aufgaben muss lediglich die Datei [`.gitlab-ci.yml`](https://gitlab.com/devops-infomgmt/aufgabe-pipeline/-/blob/master/.gitlab-ci.yml) bearbeitet werden.
Du kannst entweder das Projekt mit git clonen, danach Änderungen einpflegen und mit git wieder auf Gitlab pushen oder die Datei direkt auf Gitlab bearbeiten.

> Hinweis: Falls du die Versionsverwaltung [git](http://git-scm.com/) nicht kennst, lohnt es sich vielleicht das mal auszuprobieren. Zur Vereinfachung kannst du auch auch eine grafische Oberfläche wie [GitExtensions](http://gitextensions.github.io/) verwenden.

## Aufgabe 2: Schau dir die existierende Pipeline an

- [ ] Schau dir die bereits existierende [`latest` Pipeline](https://gitlab.com/devops-infomgmt/aufgabe-pipeline/pipelines/latest) an. Was wird hier gemacht?
- [ ] Die komplette Pipeline wird in der Datei [`.gitlab-ci.yml`](https://gitlab.com/devops-infomgmt/aufgabe-pipeline/-/blob/master/.gitlab-ci.yml) definiert. Versuche zu verstehen was die einzelnen Zeilen bedeuten.

> Hinweis: Du kannst dir dazu gerne die [Dokumentation](https://docs.gitlab.com/ee/ci/yaml/) zu Hilfe nehmen.

## Aufgabe 3: Test

Ab jetzt arbeitest du in dem von dir geforkten Projekt.

- [ ] Ändere den Job hello_world so ab, dass stattdessen das Projekt getestet wird (Ausführen von `npm run test-ci`)

<details>
<summary markdown="span">Vorgehen</summary>

Dafür muss ein anderes [Docker](www.docker.com) Image ausgewählt werden, in dem der Job ausgeführt ist.
Da es sich bei unserem Beispielprojekt um ein [React](https://reactjs.org/) Projekt handelt und das auf [Node.js](https://nodejs.org/) aufbaut, ist es sinnvoll ein [Docker Image](hub.docker.com) auszuwählen, indem bereits Node.js vorinstalliert ist.
Hier würde sich beispielsweise das Image [`node`](https://hub.docker.com/_/node) eignen.

Das git Repository wird bei Start des Jobs automatisch gepullt, daher sind alle notwendigen Dateien aus dem Repository direkt vorhanden.
Um einen Test ausführen zu können, müssen erstmal alle Abhängigkeiten installiert werden, das ist mit `npm install` möglich.
Danach können die Tests über den Befehl `npm run test-ci` gestartet werden.

</details>

> Hinweis: Du kannst die Syntax der Datei [`.gitlab-ci.yml`](https://gitlab.com/devops-infomgmt/aufgabe-pipeline/-/blob/master/.gitlab-ci.yml) jederzeit mit dem [Linter](https://gitlab.com/devops-infomgmt/aufgabe-pipeline/-/ci/lint) überprüfen.

## Aufgabe 4: Build

- [ ] Füge der Pipeline einen weiteren Job hinzu, der die entsprechenden HTML Dateien mithilfe des Befhels `npm run build` baut.

<details>
<summary markdown="span">Vorgehen</summary>

Dafür benötigst du einen weiteren Job.

Da die Jobs in der Pipeline hintereinander laufen sollen musst du verschiedene [Stages](https://docs.gitlab.com/ee/ci/yaml/#stages) definieren.
Jedem Job kannst du dann eine [Stage](https://docs.gitlab.com/ee/ci/yaml/#stage) zuweisen.
Die Stages werden dann hintereinander, aber alle Jobs in einer Stage parallel ausgeführt.

Im Job muss zum geneieren der HTML Dateien der Befehl `npm run build` ausgeführt werden, vergiss nicht vorher die Abhänigkeiten zu installieren.

Die generierten HTML Dateien befinden sich im Ordner `build`.
Diese sind allerdings nach Beendigung des Jobs nicht mehr erreichbar.
Um die Dateien aus dem Job zu extrahieren muss ein [Artifact Pfad](https://docs.gitlab.com/ee/ci/yaml/#artifactspaths) definiert werden.

Nachdem der build job der Pipeline ausgeführt wurde, können die genierierten Dateien eingesehen werden (dein Gitlab Projekt > CI/CD > Pipeline auswählen > build Job auswählen > Recht auf Browse Job artifacts klicken).
Du kannst überprüfen, ob du alles richtig gemach hast, indem du dir diese Dateien anzeigen lässt.

</details>

## Aufgabe 5: Deploy

- [ ] Deploy die gebauten HTML Dateien nach [gitlab pages](https://docs.gitlab.com/ee/ci/yaml/#pages)

<details>
<summary markdown="span">Vorgehen</summary>

Dafür muss wieder ein neuer Job mit einer neuen, nachgelagerten Stage angelegt werden.
Für das Deployment zu Github Pages muss der der Job den Namen `pages` besitzen.

Bei jedem neuen Job wird neben dem git repo auch die Artifacts der vorherigen Stages heruntergeladen.
Da wir fürs Deploment lediglich die Artifacts benötigen, kann die [Variable](https://docs.gitlab.com/ee/ci/yaml/#variables) [GIT_STRATEGY](https://docs.gitlab.com/ee/ci/yaml/#git-strategy) auf `none` gesetzt werden.

Für das Deployment auf Gitlab Pages muss der `build` Ordner in `public` umbenannt werden.
Jetzt muss dieser Ordner noch als [Artifact Pfad](https://docs.gitlab.com/ee/ci/yaml/#artifactspaths) deklariert werden.

Mit der letzten Stage der Pipeline wird das Deployment durchgeführt.
Wenn das geklappt hat, wird in deinem Gitlab Projekt unter Settings > Pages ein Link angezeigt.
Klickst du darauf, sollte dir eine kleine Website angezeigt werden.

</details>

## Hilfe

Du kommst nicht weiter?
Hier eine kleine Hilfe:

<details>
<summary markdown="span">Spoiler</summary>

So eine Pipeline soll am Ende herauskommen:

![Pipeline](README-Pipeline.png)

Fall du immernoch nicht weiter kommst melde dich einfach bei uns oder spicke in der Lösung.

<details>
<summary markdown="span">Ich will spicken</summary>

Hier gehts zur [😈 Lösung 😈](https://gitlab.com/devops-infomgmt/aufgabe-pipeline-loesung/-/blob/master/.gitlab-ci.yml)
</details>

</details>